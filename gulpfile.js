var gulp = require("gulp");
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');

var paths = {
    main: "src/main/**/*.js",
    game: ["src/game/**/!(main)*.js", "src/game/**/main.js"]
};

gulp.task('createGame', function() {
    return gulp.src(paths.game)
        .pipe(sourcemaps.init())
        .pipe(concat('game.js'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('js'))
});

gulp.task('createMain', function() {
    return gulp.src(paths.main)
        .pipe(sourcemaps.init())
        .pipe(concat('main.js'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('js'))
});

gulp.task('js', ['createGame', 'createMain']);

gulp.task('watch', function() {
    gulp.watch(paths.game, ['createGame']);
    gulp.watch(paths.main, ['createMain']);
});