var express = require('express');
var http = require('http');
var serveStatic = require('serve-static');

var viewDirectory = __dirname + '/';

var app = express();

app.use(serveStatic(viewDirectory));

http.createServer(app).listen(3001, '0.0.0.0', function () {
    console.log('Node server: start on 127.0.0.1:3001');
});

app.listen(3000, function () {
    console.log('Go, Go JJ!');
});