function Stages() {
    var config = {
        resolution: [1920, 1080],
        states: {
            active: 'home'
        },
        preloaderTexts: [
            "Chodźmy.",
            "Noc idzie.",
            "Mrok na polu zgęstniał.",
            "Czas zakosztować uroków zwycięstwa!"
        ]
    };

    var $stages = document.getElementById('stages');

    var getWindowSize = function () {
        return {
            width: window.innerWidth,
            height: window.innerHeight
        }
    };

    var setStagesSize = function () {
        var sizes = getWindowSize();
        var proportions = (sizes.width / config.resolution[0]) / (sizes.height / config.resolution[1]);
        var scaleChange = proportions > 1 ? sizes.height / config.resolution[1] : sizes.width / config.resolution[0];
        if (proportions > 1) {
            var widthChange = (sizes.width / scaleChange) / 2 - config.resolution[0] / 2;
            $stages.style.transform = "scale(" + (scaleChange) + ") translateX(" + widthChange + "px)";
        } else {
            var heightChange = (sizes.height / scaleChange) / 2 - config.resolution[1] / 2;
            $stages.style.transform = "scale(" + (scaleChange) + ") translateY(" + heightChange + "px)";
        }
    };

    setStagesSize();
    window.addEventListener('resize', setStagesSize);

    this.$stages = $stages;
    this.config = config;
    this.preloader(0);
}

Stages.prototype.goToStage = function (id) {
    var self = this;
    var $stageTarget = document.getElementById(id);
    if ($stageTarget) {
        var sections = self.$stages.getElementsByTagName('section');
        for (var s = 0, sl = sections.length; s < sl; ++s) {
            sections[s].className = "";
        }
        $stageTarget.className = 'active';
    }
};

Stages.prototype.preloader = function (progress) {
    var self = this;
    var p = !isNaN(progress) && progress > 0 ? progress < 100 ? progress : 100 : 0;
    var $progressBar = self.$stages.getElementsByClassName('progressBar')[0];
    var $bar = $progressBar.getElementsByClassName('bar')[0];
    var $progress = $bar.getElementsByClassName('progress')[0];
    var $btns = $progressBar.getElementsByClassName('startTheGame');
    var $text = $progressBar.getElementsByClassName('texts')[0];

    $progress.style.width = p + '%';
    var text = Math.floor(progress / (100 / (self.config.preloaderTexts.length - 1)));
    $text.innerHTML = self.config.preloaderTexts[text];

    var levels = [4, 6, 8];

    if (progress == 100) {
        console.log($btns);
        for (var i = 0; i < $btns.length; i++) {
            (function(index){
                $bar.className = "bar loaded";
                $btns[i].className = "startTheGame loaded";

                $btns[i].addEventListener('click', function () {
                    Interface.goToStage("game");
                    state.set("numberOfKeys", levels[index]);
                    startGame();
                });
            })(i);
        }
    }
};


Stages.prototype.finish = function (result, time, percent) {
    if (!finished) {
        finished = true;
        game.destroy();
        this.goToStage('finish');
        var $element;
        var $screen = document.getElementById('finish');
        if (result === 'win') {
            $element = document.getElementById('win');
            percent = 100;
        } else {
            $element = document.getElementById('failure');
            time = config.daytimelength / 1000
        }

        $screen.getElementsByClassName('seconds')[0].innerHTML = time;
        $screen.getElementsByClassName('percent')[0].innerHTML = percent;
        $element.className = 'active';
    }
};


var Interface = new Stages();
var finished = false;

// add stage change listeners
//var stageChangers = document.getElementsByClassName('stageChange');
//for (var s = 0, sl = stageChangers.length; s < sl; ++s) {
//    stageChangers[s].addEventListener('click', function (e) {
//        Interface.goToStage(this.hash.replace("#", ""));
//        e.preventDefault();
//    })
//}
//
//// add loading change listeners(?)
//var loaderChangers = document.getElementsByClassName('progressChange');
//var progress = 0;
//for (var l = 0, ll = loaderChangers.length; l < ll; ++l) {
//    loaderChangers[l].addEventListener('click', function (e) {
//        switch (this.hash.replace("#", "")) {
//            case "finish":
//                progress = 100;
//                break;
//            case "up":
//                progress = progress + 10;
//                break;
//            default:
//                progress = 0;
//        }
//        Interface.preloader(progress);
//        e.preventDefault();
//    })
//}


// open/hide debug
//var debugOpen = false;
//document.getElementById('debug-opener').addEventListener('click', function (e) {
//    debugOpen = !debugOpen;
//    document.getElementById('debug').className = debugOpen ? 'open' : '';
//    e.preventDefault();
//});

