var PointsBar = {

    barA : 0,
    barB : 0,


    preload: function(){

        game.load.image('bar', 'assets/img/bar.png');
        game.load.image('barbg', 'assets/img/barbg.png');
    },

    create: function(){
        this.barA = game.add.sprite(40, 40, 'barbg');
        this.barA.height=1000;
        this.barA.width=80;
        this.barB = game.add.sprite(40, 40, 'bar');
        this.barB.height=0;
        this.barB.width=80;
    },

    update: function(){
        this.barB.height=10*state.percent;
        this.barB.y=40+(1000-10*state.percent);

        if(state.percent == 100) {

            state.status = 'finished';

            Interface.finish('win', parseFloat((game.time.now - game.time.pauseDuration)/1000).toFixed(2), 100);
        }
    },

    render: function () {
    }
};