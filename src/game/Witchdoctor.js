var Witchdoctor = {
    man : 0,
    shadow: 0,

    preload: function(){
        game.load.image('shadow', 'assets/img/shadow.png');


        for (var i = 0; i < config.keys.length; i++) {
            game.load.image(config.keys[i].szaman, 'assets/img/' + config.keys[i].szaman + '.png');
        }
    },

    getWhichDoctorImg: function(){

        var img = null;
        for (var i =0, il = config.keys.length; i < il; ++i) {
            if(config.keys[i].id === state.lastKey) {
                img = config.keys[i].szaman
            }
        }

        return img;
    },

    create: function(){
        this.man = game.add.sprite(config.positionsWD[0].x, config.positionsWD[0].y, this.getWhichDoctorImg());
        this.shadow = game.add.sprite(config.positionsWD[0].shadow.x, config.positionsWD[0].shadow.y, 'shadow');
        layers.ground.add(this.shadow);
        layers.ground.add(this.man);
    },

    update: function(){
        //this.man = game

        //console.log(config.keys[state.lastKey].szaman)

        if(this.man.loadTexture) {
            this.man.loadTexture(this.getWhichDoctorImg(), 0);
        }
    },

    render: function(){

    },

    go: function(){
        state.witchdoctor.position=(state.witchdoctor.position+1)%8;
        this.man.x = config.positionsWD[state.witchdoctor.position].x;
        this.man.y = config.positionsWD[state.witchdoctor.position].y;

        if(config.positionsWD[state.witchdoctor.position].fore){
            layers.fireplace.add(this.man);
        } else {
            layers.ground.add(this.man);
        }
        console.log(config.positionsWD[state.witchdoctor.position].scale);
        this.man.scale.x = config.positionsWD[state.witchdoctor.position].scale;
        this.man.scale.y = config.positionsWD[state.witchdoctor.position].scale;

        this.shadow.x = config.positionsWD[state.witchdoctor.position].shadow.x;
        this.shadow.y = config.positionsWD[state.witchdoctor.position].shadow.y;

        this.shadow.scale.x = config.positionsWD[state.witchdoctor.position].scale;
        this.shadow.scale.y = config.positionsWD[state.witchdoctor.position].scale;
    }
};