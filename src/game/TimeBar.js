var TimeBar = {
    preload: function() {
        game.load.image('sun', 'assets/img/sun.png');
        game.load.image('night', 'assets/img/night.png');
    },
    create: function(){
        this.sun = game.add.sprite(-400, 340, 'sun');
        this.night = game.add.sprite(0, 0, 'night');
        this.sun.alpha = 0;
        layers.sky.add(this.sun);
        layers.weather.add(this.night);
        this.sunrise();

        game.time.slowMotion = 1;
    },
    sunrise: function() {
        var self = this;
        game.add.tween(self.sun).to({x:760}, config.daytimelength/2, Phaser.Easing.Linear.None, true).onComplete.addOnce(self.sunset, this);
        game.add.tween(self.sun).to({y:0}, config.daytimelength/2, Phaser.Easing.Sinusoidal.Out, true);
        game.add.tween(self.sun).to({alpha:1}, config.daytimelength/4, Phaser.Easing.Sinusoidal.Out, true);
        game.add.tween(self.night).to({alpha:0}, config.daytimelength/4, Phaser.Easing.Sinusoidal.Out, true)
    },
    sunset: function() {
        var self = this;
        game.add.tween(self.sun).to({x:1920}, config.daytimelength/2, Phaser.Easing.Linear.None, true).onComplete.addOnce(self.dayEnd, this);
        game.add.tween(self.sun).to({y:340}, config.daytimelength/2, Phaser.Easing.Sinusoidal.In, true);
        game.add.tween(self.sun).to({alpha: 0}, config.daytimelength/4, Phaser.Easing.Sinusoidal.In, true, config.daytimelength/4);
        game.add.tween(self.night).to({alpha: 1}, config.daytimelength/4, Phaser.Easing.Sinusoidal.In, true, config.daytimelength/8);
    },
    dayEnd: function() {
        state.status = 'finished';
        Interface.finish('failure', config.daytimelength/10000, state.percent);
    }
};