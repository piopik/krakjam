var Environment = {
    preload: function(){

        game.load.image('first-plan', 'assets/img/background-firstplan.png');
        game.load.image('main-plan', 'assets/img/background-mainplan.png');
        game.load.image('second-plan', 'assets/img/background-secondplan.jpg');
    },
    create: function(){
        var second = new Phaser.Sprite(game, 0, 0, 'second-plan');
        var main = new Phaser.Sprite(game, 0, 0, 'main-plan');
        var first = new Phaser.Sprite(game, 0, 0, 'first-plan');

        layers.sky.add(second);
        layers.ground.add(main);
        layers.weather.add(first);
    },

    update: function(){

    }
};