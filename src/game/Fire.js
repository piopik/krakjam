var Fire = {
    position: {x:0,y:0},
    preload: function () {
        game.load.spritesheet('fire', 'assets/img/ognisko/ognisko_pasek.png', 500, 600, 7);
        game.load.image('fireBase', 'assets/img/ognisko/4.png');
    },

    create: function () {
        this.position = {x: game.world.width / 2-100, y: game.world.height-450};

        var fire = game.add.sprite(this.position.x, this.position.y, 'fire');
        var fireBase = game.add.sprite(this.position.x, this.position.y, 'fireBase');

        fire.scale.x = 0.3;
        fire.scale.y = 0.3;

        fireBase.scale.x = 0.3;
        fireBase.scale.y = 0.3;

        layers.fireplace.add(fireBase);
        layers.fireplace.add(fire);

        var walk = fire.animations.add('fired');
        fire.animations.play('fired', 10, true);
    },

    update: function () {

    }
};