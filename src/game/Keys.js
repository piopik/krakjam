var Keys = {
    preload: function () {

        for (var i = 0; i < config.keys.length; i++) {

            game.load.image(config.keys[i].img, 'assets/img/' + config.keys[i].img + '.png');
        }
    },

    create: function () {

        var positionMultiplier = 0.1;

        function shuffle(o){ for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x); return o; }

        shuffle(config.keys);

        for (var i =0; i < state.numberOfKeys; i++) {

            state.keys.push(config.keys[i]);
        }

        for (var j = 0; j < state.keys.length; j++) {

            game.add.text(game.world.width * positionMultiplier + 60,game.world.height - 250, config.keyboard[j].key, {font:"45px Arial"});
            game.add.sprite(game.world.width * positionMultiplier, game.world.height - 200, state.keys[j].img);
            positionMultiplier += 1/state.keys.length-0.02;
        }
    },

    update: function () {

    },
};