    var game = new Phaser.Game(1920, 1080, Phaser.AUTO, 'game', { preload: preload, create: create, update: update, render: render });

    var config = {
        daytimelength : 60000,
        keys: [
            {id: 1, img: 'button-01', szaman: 'szaman-01'},
            {id: 2, img: 'button-02', szaman: 'szaman-02'},
            {id: 3, img: 'button-03', szaman: 'szaman-03'},
            {id: 4, img: 'button-04', szaman: 'szaman-04'},
            {id: 5, img: 'button-05', szaman: 'szaman-05'},
            {id: 6, img: 'button-06', szaman: 'szaman-06'},
            {id: 7, img: 'button-07', szaman: 'szaman-07'},
            {id: 8, img: 'button-08', szaman: 'szaman-08'}
        ],
        positionsWD: [
            {x:690,y:600,scale:0.85,fore:0,shadow:{x:590,y:800}},
            {x:740,y:600,scale:1,fore:1,shadow:{x:700,y:800}},
            {x:890,y:600,scale:1,fore:1,shadow:{x:890,y:800}},
            {x:1040,y:600,scale:1,fore:1,shadow:{x:1040,y:800}},
            {x:1090,y:600,scale:0.85,fore:0,shadow:{x:1090,y:800}},
            {x:1040,y:600,scale:0.7,fore:0,shadow:{x:1040,y:700}},
            {x:890,y:600,scale:0.7,fore:0,shadow:{x:890,y:700}},
            {x:740,y:600,scale:0.7,fore:0,shadow:{x:690,y:700}}
        ],
        keyboard: [
            {key: 'A'},
            {key: 'S'},
            {key: 'D'},
            {key: 'F'},
            {key: 'H'},
            {key: 'J'},
            {key: 'K'},
            {key: 'L'}

        ]
    };

    var state = {
        set : function(name,value){
            if(this[name]!==value){
                this[name]=value;
            }
        },
        percent : 0,
        name: 'John',
        nextKey: 8,
        lastKey: 1,
        keys: [],
        comboCounter:1,
        witchdoctor: {
            position: 0,
            dance: 0
        },
        numberOfKeys: 8,
        status: 'notStarted',
        timestamp: 0
    };

    var lastTimestamp=state.timestamp;

    var layers= {};

    function preload() {

        game.load.onFileComplete.add(fileComplete, this);

        PointsBar.preload();
        Keys.preload();
        Controller.preload();
        Environment.preload();
        BgMusic.preload();
        TimeBar.preload();
        Witchdoctor.preload();
        Rain.preload();
        Fire.preload();

    }

    //	This callback is sent the following parameters:
    function fileComplete(progress) {
        Interface.preloader(progress)
    }
    function create() {

        state.status = 'inProgress';

        //game.add.plugin(Phaser.Plugin.Debug);

        layers = {
            sky: game.add.group(),
            ground: game.add.group(),
            fireplace: game.add.group(),
            weather: game.add.group(),
            interface: game.add.group()
        };
        layers.sky.z = 0;
        layers.ground.z = 10;
        layers.fireplace.z = 50;
        layers.weather.z = 100;
        layers.interface.z = 1000;

    }

    function startGame() {
        Environment.create();
        TimeBar.create();
        Keys.create();
        NextKey.create();
        PointsBar.create();
        Witchdoctor.create();
        BgMusic.create();
        Controller.create();
        Fire.create();
        Rain.create();
    }


    function update() {
        if(state.status == 'inProgress') {
            //Environment.update();
            //Keys.update();
            //NextKey.update();
            PointsBar.update();
            Rain.update();
            Witchdoctor.update();
        }
    }

    function render(){
    }

