var NextKey = {
    position: {x: 0, y: 0},
    sprite: undefined,

    change: function () {

        var keys = state.keys,
            random = Math.floor((Math.random() * keys.length)),
            nextKeyId = keys[random].id,
            key = state.keys[random].img;

        state.set('nextKey', nextKeyId);
        this.sprite.key = key;
        this.sprite.loadTexture(key, 0);

    },
    create: function () {
        this.position = {x: game.world.width / 2 - 100, y: 40};
        this.sprite = game.add.sprite(this.position.x, this.position.y, state.keys[Math.floor((Math.random() * state.keys.length))].img);
    },

    update: function () {

    }
};