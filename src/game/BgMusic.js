var bgMusic;

var BgMusic = {

    preload: function() {

        game.load.audio('bg_music', 'assets/sound/bg_music.wav');
    },

    create: function(){

        bgMusic = game.add.audio('bg_music');
        bgMusic.loop = true;
        bgMusic.play();
        bgMusic.volume=0.5;


    },

    update: function(){

    }
};