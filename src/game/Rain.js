var Rain = {

    emitter: undefined,
    rainFX: undefined,

    preload: function () {
        this.emitter = game.add.emitter(game.world.centerX, 0, 1000);
        game.load.spritesheet('drop', 'assets/img/drop2.png', 17, 17);
        game.load.audio('rainFX', 'assets/sound/rain.mp3');
    },

    create: function(){
        rainFX = game.add.audio('rainFX');
        rainFX.loop = true;
        rainFX.play();
        rainFX.volume=0;
    },

    add: function () {

        emitter = game.add.emitter(game.world.centerX, 0, 1000);
        layers.weather.add(emitter);

        emitter.width = game.world.width;
        //this.emitter.angle = 30; // uncomment to set an angle for the rain.

        emitter.makeParticles('drop');

        emitter.minParticleScale = 0.5;
        emitter.maxParticleScale = 1.5;

        emitter.setYSpeed(300, 500);
        emitter.setXSpeed(-5, 5);

        emitter.minRotation = 0;
        emitter.maxRotation = 0;

        emitter.start(false, 1600, 5);
    },
    counter: 0,
    update: function () {

        if (state.percent >= 10 && this.counter == 0) {
            this.add();
            rainFX.volume=0.2;
            this.counter++
        }if(state.percent >= 30 && this.counter == 1){
            this.add();
            rainFX.volume=0.4;
            this.counter ++
        }if(state.percent >= 50 && this.counter == 2){
            this.add();
            rainFX.volume=0.6;
            this.counter ++
        }if(state.percent >= 70 && this.counter == 3){
            this.add();
            rainFX.volume=0.8;
            this.counter ++
        }if(state.percent >= 90 && this.counter == 4){
            this.add();
            rainFX.volume=1;
            this.counter ++
        }if(state.percent >= 100 && this.counter == 5){
            this.add();
            this.counter ++
        }
    }
};

