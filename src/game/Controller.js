var fx;

var Controller = {
    comboBar: 0,

    preload: function(){

        game.load.audio('buttonSfx', 'assets/sound/shaman_voice01.wav');

    },
    create: function(){

        fx = game.add.audio('buttonSfx');

        Controller.comboBar= game.add.text(game.world.centerX, 300, "", {
            font: "50px Arial",
            fill: "#ffffff",
            align: "center"
        });

        Controller.comboBar.anchor.setTo(0.5, 0.5);


        layers.interface.add(this.comboBar);

        document.onkeydown = checkKey;

        function checkKey(e) {
            var id= null;
            e = e || window.event;

            switch(e.keyCode){
                case 65:
                    id=0;
                    break;
                case 83:
                    id=1;
                    break;
                case 68:
                    id=2;
                    break;
                case 70:
                    id=3;
                    break;
                case 72:
                    id=4;
                    break;
                case 74:
                    id=5;
                    break;
                case 75:
                    id=6;
                    break;
                case 76:
                    id=7;
                    break;
                default:
                    id=null;
                    break;
            }
                if(id!==null){
                    state.lastKey = config.keys[id].id;
                    if(state.nextKey==config.keys[id].id){

                        if(game.time.totalElapsedSeconds()<(state.timestamp+3)){
                            state.comboCounter=1.1*state.comboCounter;
                            if(state.comboCounter>3){
                                state.comboCounter=3;
                            }


                        } else {
                            state.comboCounter=1;
                        }



                        state.set('percent',state.percent+Math.floor(state.comboCounter));
                        //Controller.comboBar.key='comboBar'+Math.floor(state.comboCounter);


                        if(state.percent >100){
                            state.set('percent',100);
                        }

                        fx.play();

                        bgMusic.volume=(0.5+0.005*state.percent);

                        Witchdoctor.go();
                        NextKey.change();
                    } else {
                        state.set('percent', state.percent - 2);
                        if(state.percent < 0 ) {
                            state.set('percent', 0);
                        }
                        state.comboCounter=1;
                        NextKey.change();
                    }
                }

                if(state.comboCounter<2){
                    Controller.comboBar.setText("");
                } else {
                    Controller.comboBar.setText("COMBO x"+Math.floor(state.comboCounter)+" !");
                }
                state.timestamp=game.time.totalElapsedSeconds();
        }
    },
    update: function(){

    }
};